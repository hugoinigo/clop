#ifndef __LOP_H
#define __LOP_H
#include <stdint.h>
#include <stdio.h>

struct lop_instance {
  size_t n;
  int64_t *matrix;
};

uint64_t lop_ncalls();
void lop_reset_ncalls();

int64_t lop_obj_func(const struct lop_instance *inst,
		     uint32_t *vec,
		     uint32_t n);

int64_t lop_obj_part(const struct lop_instance *inst,
		     uint32_t ind,
		     uint32_t pos);

int32_t lop_inst_parse(FILE *f, struct lop_instance *inst);

void lop_inst_dbg(const struct lop_instance *inst);
int32_t lop_obj_estim(const struct lop_instance *inst,
                      int64_t *upper,
                      int64_t *lower);
void lop_inst_free(struct lop_instance *inst);
int32_t lop_becker_construct(int32_t *dest, const struct lop_instance *inst);
int32_t lop_our_construct(int32_t *dest, const struct lop_instance *inst);
#endif /* __LOP_H */
