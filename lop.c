#include "lop.h"
#include <stdlib.h>
#include <errno.h>
#include <string.h>

static uint64_t obj_func_ncalls = 0;

int32_t cmp_dbl(const void *a, const void *b)
{
	if ((*(double*) a) - (*(double*) b)) {
		return -1;
	} else {
		return 1;
	}
}

int32_t cmp_int64(const void *a, const void *b)
{
	if ((*(int64_t*) a) - (*(int64_t*) b)) {
		return -1;
	} else {
		return 1;
	}
}

int32_t lop_becker_construct(int32_t *dest, const struct lop_instance *inst)
{
	int32_t res, i, j;
	int64_t num_a, num_b;
	double *aux;
	size_t n = inst->n;
	
	aux = malloc((sizeof(double) + sizeof(int64_t)) * inst->n);
	if (aux == NULL) {
		res = -1;
		goto error;
	}

	for (i = 0; i < n; i++) {
		num_a = 0;
		num_b = 0;
		for (j = 0; j < n; j++) {
			num_a += inst->matrix[i*n + j];
			num_b += inst->matrix[j*n + i];
		}
		aux[i*2+1] = i;
		aux[i*2] = (double) num_a / num_b;
	}
	qsort(aux, n, sizeof(double) + sizeof(int64_t), cmp_dbl);
	for (i = 0; i < n; i++) {
		dest[i] = aux[i*2+1];
	}
	res = 0;
 err_f:
	free(aux);
 error:
	return res;
}

int32_t lop_our_construct(int32_t *dest, const struct lop_instance *inst)
{
	int32_t res, i, j;
	int64_t num_a, num_b;
	int64_t *aux;
	size_t n = inst->n;

	aux = malloc(sizeof(double) * 2 * inst->n);
	if (aux == NULL) {
		res = -1;
		goto error;
	}
	for (i = 0; i < n; i++) {
		num_a = 0;
		num_b = 0;
		for (j = 0; j < n; j++) {
			num_a += inst->matrix[i*n + j];
			num_b += inst->matrix[j*n + i];
		}
		aux[i*2+1] = i;
		aux[i*2] = num_a - num_b;
	}
	qsort(aux, n, sizeof(int64_t) * 2, cmp_int64);
	for (i = 0; i < n; i++) {
		dest[i] = aux[i*2+1];
	}
	res = 0;
	free(aux);
 error:
	return res;
}

uint64_t lop_ncalls()
{
	return obj_func_ncalls;
}

void lop_reset_ncalls()
{
	obj_func_ncalls = 0;
}

int64_t lop_obj_func(const struct lop_instance *inst,
                     uint32_t *vec,
                     uint32_t n)
{
        int64_t solution;
        uint32_t i, j;
        uint32_t i_ind, j_ind;
        int64_t *mat;

	obj_func_ncalls += 1;
        mat = inst->matrix;
        solution = 0;

        for (i = 0; i < n; i++) {
                i_ind = vec[i];
                for (j = i+1; j < n; j++) {
                        j_ind = vec[j];
                        solution += mat[n*i_ind + j_ind];
                }
        }

        return solution;
}

size_t binom_coef(size_t n)
{
        return (n*n - n) / 2;
}

int32_t lop_obj_estim(const struct lop_instance *inst,
                      int64_t *upper,
                      int64_t *lower)
{
        int64_t *aux;
        size_t i;
        int32_t res = 0;
        size_t len = inst->n * inst->n;
        size_t coef = binom_coef(inst->n);

        int64_t upp = 0;
        int64_t low = 0;

        aux = malloc(sizeof(int64_t) * len);
        if (aux == NULL) {
                res = -1;
                goto malloc_error;
        }

        memcpy(aux, inst->matrix, len);
	qsort(aux, len, sizeof(int64_t), cmp_dbl);

        for (i = 0; i < coef; i++) {
                upp += aux[i];
                low += aux[i + coef];
        }

        free(aux);
        *upper = upp;
        *lower = low;

 malloc_error:
        return res;
}

int32_t lop_inst_parse(FILE *f, struct lop_instance *inst)
{
        size_t n, i;
        int64_t *mat, num;
        int32_t ret;
        
        rewind(f);
        ret = fscanf(f, "%ld", &n);
        if (ret != 1) {
                goto error1;
        }

        mat = malloc(n * n * sizeof(int64_t));
        for (i = 0; i < n*n; i++) {
                ret = fscanf(f, "%ld", &num);
                if (ret != 1) {
                        goto error0;
                }
                mat[i] = num;
        }

        inst->matrix = mat;
        inst->n = n;
        return 0;
 error0:
        free(mat);
 error1:
        fprintf(stderr, "wrong file format\n");
        return ret;
}

void lop_inst_dbg(const struct lop_instance *inst)
{
        size_t i;
        size_t size;
        
        printf("%ld", inst->n);
        size = inst->n * inst->n;
        for (i = 0; i < size; i++) {
                if (i % inst->n == 0) {
                        printf("\n");
                }
                printf("%4ld ", inst->matrix[i]);
        }
        printf("\n");
}

void lop_inst_free(struct lop_instance *inst)
{
        free(inst->matrix);
}

