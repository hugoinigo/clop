#include "neighborhood.h"
#include "lop.h"
#include "permutation.h"

#include <malloc.h>
#include <math.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <stdbool.h>

#define DEF_BETA 0.9

double temp_init(int32_t f_max,
		 int32_t f_min,
		 double accept_ratio)
{
	return (double) (f_min - f_max) / log(accept_ratio);
}

static inline double temp_update_geom(double temp, double beta)
{
	return temp * beta;
}

static inline double temp_update_lin(double temp, double beta)
{
	return temp - beta;
}

static inline double temp_update_log(double init_temp, double iter)
{
	return init_temp / log(iter);
}

int32_t neigh_rand(int32_t *permu, size_t len)
{
	int32_t res = 0;
	size_t i, j;
	i = rand() % len;
	do {
		j = rand() % len;
	} while (j == i);

	permu_insert(permu, len, i, j);
	return res;
}

int32_t neigh_simulated_annealing(const struct lop_instance *inst,
				  int32_t *result,
				  size_t len,
				  int32_t iter_max,
				  int32_t chain_max)
{
	double temp;
	int32_t chain_size;
	int32_t num_iter;

        int64_t f_upper, f_lower;
	int32_t f_new, f_prev, f_delta, f_best;
	double l, aux;
	int32_t *permu_new, *permu_prev;

	// size_t x_new = 1;
        int32_t res = 0;
	bool stop = false;

        permu_new = malloc(sizeof(int32_t) * len);
        if (permu_new == NULL) {
                res = -1;
                goto malloc_error;
        }
	permu_prev = malloc(sizeof(int32_t) * len);
        if (permu_prev == NULL) {
                res = -1;
                goto malloc_error;
        }
	// memcpy(buf, (int32_t* []) {permu_prev, permu_new}, sizeof buf);

	permu_ident(permu_new, len);
	permu_rand(permu_new, len);

	permu_copy(permu_prev, permu_new, len);

	permu_dbg("prev", permu_prev, len);
	permu_dbg("new", permu_new, len);

	f_prev = lop_obj_func(inst, permu_prev, len);
	f_best = f_prev;
	
	// chain_max = 1000;
	// iter_max = 5000;
	num_iter = 0;
	chain_size = 0;

        lop_obj_estim(inst, &f_upper, &f_lower);
	temp = temp_init(f_upper, f_lower, 0.75);
	int32_t better = 0, worse = 0;
	// printf("temp %f\n", temp);
	// printf("temp %f\n", temp_update_geom(temp, DEF_BETA));

	while (num_iter < iter_max && !stop) {
		chain_size = 0;
		while (chain_size < chain_max) {
			neigh_rand(permu_new, len);
			f_new = lop_obj_func(inst, permu_new, len);

			if (f_new > f_best) {
				f_best = f_new;
				permu_copy(result, permu_new, len);
				printf("SA new best (%d)", f_best);
				permu_dbg(" ", result, len);
			}

			f_delta = f_new - f_prev;
			if (f_delta > 0) {
				better++;
				f_prev = f_new;
				permu_copy(permu_prev, permu_new, len);
			} else {
				l = (double) rand() / (double) RAND_MAX;
				aux = - (double) f_delta / temp;
				if (exp(aux) > l) {
					worse++;
					f_prev = f_new;
					permu_copy(permu_prev, permu_new, len);
				} else {
					permu_copy(permu_new, permu_prev, len);
				}
			}
			chain_size += 1;
			if (lop_ncalls() >= chain_max*iter_max) {
				stop = true;
				break;
			}
		}
		temp = temp_update_geom(temp, DEF_BETA);
		if (temp == 0.f) {
			break;
		}
		//printf("temp %f\n", temp);
		num_iter += 1;
	}

	//printf("better: %d\nworse: %d\n", better, worse);
	if (f_best < f_prev) {
		permu_copy(result, permu_prev, len);
	}
	if (f_best < f_new) {
		permu_copy(result, permu_new, len);
	}
	//printf("lop sa prev: %lu\n", lop_obj_func(inst, permu_prev, len));
	//printf("lop sa new: %lu\n", lop_obj_func(inst, permu_new, len));
        free(permu_new);
	free(permu_prev);
 malloc_error:
        return res;
}
