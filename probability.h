#ifndef __PROB_H
#define __PROB_H
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

bool prob_bernoulli(float prob);
size_t prob_vec(const float *vec, size_t len);
void prob_init_state(uint32_t seed);

#endif /* __PROB_H */
