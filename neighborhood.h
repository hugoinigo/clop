#ifndef __NEIGHBORHOOD_H
#define __NEIGHBORHOOD_H
#include "lop.h"

#include <stdint.h>
#include <stdio.h>

int32_t neigh_simulated_annealing(const struct lop_instance *inst,
				  int32_t *result,
				  size_t len,
				  int32_t iter_max,
				  int32_t chain_max);

#endif /* __NEIGHBORHOOD_H */
