#include "probability.h"
#include <stdlib.h>

static uint32_t rand_calls = 0;

void prob_init_state(uint32_t seed)
{
        srand(seed);
}

bool prob_bernoulli(float prob)
{
        float res = (float) rand() / RAND_MAX;
        rand_calls += 1;
        
        return res < prob;
}

size_t prob_vec(const float *vec, size_t len)
{
        size_t i;
        float acc = 0;
        float res = (float) rand() / RAND_MAX;
        rand_calls += 1;

        for (i = 0; i < len; i++) {
                acc += vec[i];
                if (res < acc) {
                        return i;
                }
        }
        return len - 1;
}



