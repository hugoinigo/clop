#include "lop.h"
#include "permutation.h"
#include "neighborhood.h"
#include "genetic.h"
#include <errno.h>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>

int32_t main(int32_t argc, char *argv[])
{
        char *prog_name, *file_name;
        FILE *f;
        struct lop_instance inst;
        int32_t ret;
	int32_t seed;
	int32_t iter_max, chain_size;

        int32_t *ga, *sa;
        size_t n;
	char f_out[200];
	// int32_t best[50] = {7, 48, 49, 33, 38, 50, 11, 28, 25, 27, 41, 45, 37, 23, 22, 20, 24, 26, 42, 43, 1, 5, 21, 39, 34, 19, 15, 18, 16, 17, 14, 32, 8, 9, 10, 35, 36, 30, 46, 29, 31, 13, 12, 3, 44, 2, 40, 4, 6, 47};        
        prog_name = argv[0];
        if (argc != 5) {
                fprintf(stderr, "usage: `%s <filename> <seed> <iter_max> <chain_size>'\n", prog_name);
                return 1;
        }
        file_name = argv[1];
        printf("filename: `%s'\n", file_name);
        f = fopen(file_name, "r");
        if (f == NULL) {
                perror("fopen");
                return errno;
        }
	seed = atoi(argv[2]);
	iter_max = atoi(argv[3]);
	chain_size = atoi(argv[4]);

        ret = lop_inst_parse(f, &inst);
	fclose(f);
        if (ret != 0) {
                return ret;
        }
        lop_inst_dbg(&inst);

        n = inst.n;
        // printf("%d! = %lu\n", 50, permu_fact(50));

        // --- scratch ---
	// int32_t proba[9] = {8, 2, 6, 7, 1, 5, 4, 0, 3};
	// int32_t child[9] = {0};
        sa = malloc(sizeof(int32_t) * n);
	ga = malloc(sizeof(int32_t) * n);
        permu_ident(sa, n);
	// gen_pmx(child, ident, proba, 9);

	// permu_dbg("child", child, 9);
	permu_reverse(sa, n);
        permu_dbg("reverse", sa, n);

        printf("lop obj ident: %lu\n", lop_obj_func(&inst, sa, n));
	lop_becker_construct(sa, &inst);
	permu_dbg("becker", sa, n);
	printf("lop obj becker: %lu\n", lop_obj_func(&inst, sa, n));

	lop_our_construct(sa, &inst);
	permu_dbg("our", sa, n);
	printf("lop obj our: %lu\n", lop_obj_func(&inst, sa, n));

	srand(seed);	

	permu_rand(sa, n);
	permu_dbg("rand", sa, n);

	lop_reset_ncalls();
	neigh_simulated_annealing(&inst, sa, n, iter_max, chain_size);
	printf("lop obj function calls: %ld\n", lop_ncalls());

	lop_reset_ncalls();
	gen_tournament(ga, &inst, n, n, 2, 0.5, iter_max*chain_size);
	permu_dbg("sim ann", sa, n);
	permu_dbg("gen alg", ga, n);
	printf("lop obj function calls: %ld\n", lop_ncalls());
	//printf("lop obj sim ann: %lu\n", lop_obj_func(&inst, sa, n));
	printf("lop obj sim ann: %lu\n", lop_obj_func(&inst, sa, n));
	printf("lop obj gen alg: %lu\n", lop_obj_func(&inst, ga, n));
	
	// permu_rand(ident, n);
	// permu_dbg("rand1", ident, n);
	// 
	// permu_rand(ident, n);
	// permu_dbg("rand2", ident, n);
        // free(ident);
	sprintf(f_out, "%s-%s-%d-%d-%d.txt", argv[0], file_name, seed, iter_max, chain_size);
	f = fopen(f_out, "w+");
        if (f == NULL) {
                perror("fopen");
                return errno;
        }
	permu_fdbg(f, "SA:", sa, n);
	permu_fdbg(f, "GA:", ga, n);

	fprintf(f, "LOP obj SA: %lu\n", lop_obj_func(&inst, sa, n));
	fprintf(f, "LOP obj GA: %lu\n", lop_obj_func(&inst, ga, n));

	// permu_dbg("best", best, n);
	// printf("lop obj best: %lu\n", lop_obj_func(&inst, &best, n));

        // ---------------
	free(sa);
	free(ga);
        lop_inst_free(&inst);
        fclose(f);
        return 0;
}
