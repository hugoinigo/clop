#include "permutation.h"
#include <malloc.h>
#include <stddef.h>
#include <stdbool.h>
#include <errno.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>

void permu_insert(int32_t *permu,
		  size_t len,
		  int32_t at,
		  int32_t to)
{
	size_t i;
	int32_t aux;

	// assert(0 < at && at <= len && "`at' out of range");
	// assert(0 < to && to <= len && "`to' out of range");
	aux = permu[at];

	if (at < to) {
		for (i = at; i < to; i++) {
			permu[i] = permu[i+1];
		}
		permu[to] = aux;
	} else {
		for (i = at; i > to; i--) {
			permu[i] = permu[i-1];
		}
		permu[to] = aux;
	}
}

void permu_swap(int32_t *permu,
		size_t len,
		int32_t ind)
{
	size_t i;
	int32_t aux;

	assert(0 <= ind && ind < len-1 && "`ind' out of range");

	aux = permu[ind];
	permu[ind] = permu[ind+1];
	permu[ind+1] = aux;
}

void permu_swap_ij(int32_t *permu,
                   size_t len,
                   int32_t i,
                   int32_t j)
{
        int32_t aux;

	assert(0 <= i && i < len && "`i' out of range");
        assert(0 <= j && j < len && "`j' out of range");

        aux = permu[i];
        permu[i] = permu[j];
        permu[j] = aux;
}

uint64_t permu_fact(uint64_t n)
{
        uint64_t i;
        uint64_t res = 1;
        
        for (i = 2; i <= n; i++) {
                res *= i;
        }
        return res;
}

void permu_copy(int32_t *dest,
                const int32_t *src,
                size_t len)
{
        memcpy(dest, src, sizeof(int32_t) * len);
}

void permu_ident(int32_t *permu, size_t len)
{
        size_t i;
        for (i = 0; i < len; i++) {
                permu[i] = i;
        }
}

void permu_inv(int32_t *permu, size_t len)
{
	size_t i;
	int32_t *aux = malloc(sizeof(int32_t) * len);
	
	for (i = 0; i < len; i++) {
		aux[permu[i]] = i;
	}
	permu_copy(permu, aux, len);
	free(aux);
}

void permu_inv_from(int32_t *dest,
		    const int32_t *src,
		    size_t len)
{
	size_t i;
	for (i = 0; i < len; i++) {
		dest[src[i]] = i;
	}
}

int32_t permu_rand2(int32_t *permu, size_t len)
{
	size_t i;
	int32_t res = 0;
	int32_t aux;

	for (i = 0; i < len; i++) {
		aux = (rand() % (len-i));
		permu[i] = aux;
	}
	return res;
}

int32_t permu_rand(int32_t *permu, size_t len)
{
        size_t i;
        int32_t ind, num;
        int32_t res = 0;
        // int32_t *aux = malloc(sizeof(int32_t) * len);
        // if (aux == NULL) {
        //         res = -1;
        //         goto malloc_error;
        // }
        // permu_ident(aux, len);

        for (i = len; i > 0; i--) {
                ind = rand() % i;
                num = permu[ind];

                permu_swap_ij(permu, len, ind, len-1);
                // permu[len-i] = num;
        }

        // free(aux);
 malloc_error:
        return res;
}

void permu_reverse(int32_t *permu, size_t len)
{
	int32_t temp;
	size_t i;
	for (i = 0; i < len/2; i++) {
		temp = permu[i];
		permu[i] = permu[len-i-1];
		permu[len-i-1] = temp;
	}
}

void permu_dbg(const char *name,
               int32_t *vec,
               size_t len)
{
        size_t i;
        printf("%s ", name);
        for (i = 0; i < len; i++) {
                printf("%d ", vec[i] + 1);
        }
        printf("\n");
}

void permu_fdbg(FILE *fp,
		const char *name,
		int32_t *vec,
		size_t len)
{
	size_t i;
        fprintf(fp, "%s ", name);
        for (i = 0; i < len; i++) {
                fprintf(fp, "%d ", vec[i] + 1);
        }
        fprintf(fp, "\n");
}

int32_t permu_check(const int32_t *permu, size_t len)
{
        size_t i;
        int32_t aux, res;
        const int32_t max = len - 1;
        int32_t *check = calloc(len, sizeof(int32_t));

        if (check == NULL) {
                res = errno;
                perror("calloc");
                goto error_aux;
        }
        
        for (i = 0; i < len; i++) {
                aux = permu[i];
                if (aux < 1 || max < aux) {
                        res = -1;
                        goto error;
                }
                check[aux] = 1;
        }

        for (i = 0; i < len; i++) {
                if (check[i] == 0) {
                        res = 0;
                        goto error;
                }
        }
        res = 1;
 error:
        free(check);
 error_aux:
        return res;
}

