CFILES=neighborhood.c largeint.c lop.c probability.c permutation.c genetic.c main.c
HFILES=neighborhood.h largeint.h lop.h probability.h permutation.h genetic.h
TARGET=clop
LFLAGS=-lm

all:
	cc -o ${TARGET} ${CFILES} ${HFILES} ${LFLAGS}
