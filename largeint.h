#ifndef __INTN_H
#define __INTN_H

#include <stdint.h>
#include <stdbool.h>
#include <stdarg.h>

struct intn {
	uint64_t *data;
	uint64_t size;
	bool sign;
};

typedef struct intn intn_t;

void intn_init(intn_t *num, uint64_t size, ...);

#endif /* __INTN_H */

