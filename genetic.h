#ifndef __GENETIC_H
#define __GENETIC_H
#include "lop.h"

#include <stdint.h>
#include <stdlib.h>

int32_t gen_rand_permu_list(int32_t ***dest,
			    size_t n,
			    size_t len);

void gen_pmx(int32_t *child,
	     const int32_t *p1,
	     const int32_t *p2,
	     size_t len,
	     int32_t scut,
	     int32_t ecut);

void gen_tournament(int32_t *result,
		    const struct lop_instance *inst,
		    size_t pop_len,
		    size_t permu_len,
		    size_t k,
		    float mut_prob,
		    size_t num_iters);

#endif /* __GENETIC_H */
