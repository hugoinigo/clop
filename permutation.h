#ifndef __PERMU_H
#define __PERMU_H
#include <stdint.h>
#include <stddef.h>
#include <stdio.h>

void permu_insert(int32_t *permu,
		  size_t len,
		  int32_t at,
		  int32_t to);

void permu_swap(int32_t *permu,
		size_t len,
		int32_t ind);

void permu_swap_ij(int32_t *permu,
                   size_t len,
                   int32_t i,
                   int32_t j);

uint64_t permu_fact(uint64_t n);
void permu_ident(int32_t *vec, size_t len);
void permu_copy(int32_t *dest,
                const int32_t *src,
                size_t len);
void permu_inv(int32_t *permu, size_t len);
void permu_inv_from(int32_t *dest,
		    const int32_t *src,
		    size_t len);
int32_t permu_rand(int32_t *permu, size_t len);
void permu_dbg(const char *name,
               int32_t *vec,
               size_t len);
void permu_fdbg(FILE *fp,
		const char *name,
		int32_t *vec,
		size_t len);
int32_t permu_check(const int32_t *vec, size_t len);
void permu_reverse(int32_t *vec, size_t len);

#endif /* __PERMU_H */
