#include "genetic.h"
#include "permutation.h"
#include "lop.h"
#include "probability.h"
// #include <stdlib.h>

int32_t gen_rand_permu_list(int32_t ***dest,
			    size_t len,
			    size_t n)
{
	size_t i;
	int32_t res = 0;
	int32_t **permu_list = malloc(sizeof(int32_t*) * len);
	if (permu_list == NULL) {
		res = -1;
		goto list_malloc;
	}

	for (i = 0; i < len; i++) {
		permu_list[i] = malloc(sizeof(int32_t) * n);
		if (permu_list[i] == NULL) {
			res = -1;
			goto permu_malloc;
		}
		permu_ident(permu_list[i], n);
		permu_rand(permu_list[i], n);
	}
	*dest = permu_list;
	goto list_malloc;

 permu_malloc:
	free(permu_list);

 list_malloc:
	return res;
}

#define GEN_ZERO -1
void gen_fill(int32_t *child, size_t len, int32_t num)
{
	size_t i;
	for (i = 0; i < len; i++) {
		child[i] = num;
	}
}

void gen_pmx(int32_t *child,
	     const int32_t *p1,
	     const int32_t *p2,
	     size_t len,
	     int32_t scut,
	     int32_t ecut)
{
	int32_t aux, aux2, ind;
	size_t i;
	int32_t *mask = calloc(sizeof(int32_t), len);
	int32_t *p2inv = malloc(sizeof(int32_t) * len);

	permu_inv_from(p2inv, p2, len);
	gen_fill(child, len, -1);

	//int32_t scut = 3;
	//int32_t ecut = 7;

	for (i = scut; i < ecut; i++) {
		aux = p1[i];
		child[i] = aux;
		mask[aux] = 1;
	}

	for (i = scut; i < ecut; i++) {
		aux = p2[i];
		if (!mask[aux]) {
			ind = i;
			do {
				aux2 = p1[ind];
				ind = p2inv[aux2];
			} while (child[ind] != GEN_ZERO);
			child[ind] = aux;
		}
	}
	for (i = 0; i < scut; i++) {
		if (child[i] == GEN_ZERO) {
			child[i] = p2[i];
		}
	}
	for (i = ecut; i < len; i++) {
		if (child[i] == GEN_ZERO) {
			child[i] = p2[i];
		}
	}
	free(mask);
	free(p2inv);
}

void gen_cuts(int32_t *scut, int32_t *ecut, size_t len)
{
	int32_t a, b;
	a = rand() % len;
	do {
		b = rand() % len;
	} while (b == a);
	if (a < b) {
		*scut = a;
		*ecut = b;
	} else {
		*scut = b;
		*ecut = a;
	}
}

void gen_mut_swap(int32_t *result, size_t len)
{
	int32_t i, j;
	i = rand() % len;
	do {
		j = rand() % len;
	} while (j == i);
	permu_swap_ij(result, len, i, j);
}

void gen_tournament(int32_t *result,
		    const struct lop_instance *inst,
		    size_t pop_len,
		    size_t permu_len,
		    size_t k,
		    float mut_prob,
		    size_t num_iters)
{
	int32_t **permu_list;
	int32_t *mask;
	int32_t *p_list, *c_list;
	int64_t *func_scores;
	size_t i, iters;
	size_t iter, j, j_max;
	int64_t f_max, f_best;
	int32_t p1, p2, c1, c2;
	int32_t scut, ecut;
	bool stop = false;

	mask = malloc(sizeof(int32_t) * pop_len);
	p_list = malloc(sizeof(int32_t) * pop_len/2);
	c_list = malloc(sizeof(int32_t) * pop_len/2);
	func_scores = malloc(sizeof(int64_t) * pop_len);

	gen_rand_permu_list(&permu_list, pop_len, permu_len);
	f_best = 0;
	for (i = 0; i < pop_len; i++) {
		func_scores[i] = lop_obj_func(inst, permu_list[i], permu_len);
		// printf("score %d", func_scores[i]);
		// permu_dbg("", permu_list[i], permu_len);
		if (func_scores[i] > f_best) {
			permu_copy(result, permu_list[i], permu_len);
			f_best = func_scores[i];
			printf("GA new best (%ld)", f_best);
			permu_dbg("", result, permu_len);
		}
	}
	while(!stop) {
		gen_fill(mask, pop_len, 0);
		for (iter = 0; iter < pop_len/2; iter++) {	
			f_max = -1;
			j_max = 0;
			for (i = 0; i < k; i++) {
				j = rand() % pop_len;
				while (mask[j]) {
					j = (j + 1) % pop_len;
				}
				if (func_scores[j] > f_max) {
					f_max = func_scores[j];
					j_max = j;
				}
			}
			mask[j_max] = 1;
		}
		// permu_dbg("mask", mask, pop_len);

		i = 0;
		j = 0;
		for (iter = 0; iter < pop_len; iter++) {
			if (mask[iter]) {
				p_list[i++] = iter;
			} else {
				c_list[j++] = iter;
			}
		}
		// permu_dbg("p_list", p_list, pop_len/2);
		// permu_dbg("c_list", c_list, pop_len/2);

		for (iter = 0; iter < pop_len/4; iter++) {
			p1 = p_list[iter*2];
			p2 = p_list[iter*2+1];
			c1 = c_list[iter*2];
			c2 = c_list[iter*2+1];

			//printf("iter %d pop_len/4 %d\n", iter, pop_len/4);

			gen_cuts(&scut, &ecut, permu_len);
			// printf("cuts %d %d\n", scut, ecut);
			gen_pmx(permu_list[c1], permu_list[p1], permu_list[p2], permu_len, scut, ecut);
			if (prob_bernoulli(mut_prob)) {
				gen_mut_swap(permu_list[c1], permu_len);
			}
			gen_pmx(permu_list[c2], permu_list[p2], permu_list[p1], permu_len, scut, ecut);
			if (prob_bernoulli(mut_prob)) {
				gen_mut_swap(permu_list[c2], permu_len);
			}
			func_scores[c1] = lop_obj_func(inst, permu_list[c1], permu_len);
			func_scores[c2] = lop_obj_func(inst, permu_list[c2], permu_len);
			if (lop_ncalls() >= num_iters) {
				stop = true;
				break;
			}
			// printf("%d c1 obj %d", iter, func_scores[c1]);
			// permu_dbg("", permu_list[c1], permu_len);
			// printf("%d c2 obj %d", iter, func_scores[c2]);
			// permu_dbg("", permu_list[c2], permu_len);
		}
		// printf("epoch %d\n", iters);
		for (iter = 0; iter < pop_len; iter++) {
			if (func_scores[iter] > f_best) {
				f_best = func_scores[iter];
				permu_copy(result, permu_list[iter], permu_len);
				printf("GA new best (%ld)", f_best);
				permu_dbg("", result, permu_len);
			}
		}
	}
	free(mask);
	free(p_list);
	free(c_list);
	free(func_scores);
	for (iter = 0; iter < pop_len; iter++) {
		free(permu_list[iter]);
	}
	free(permu_list);
}

