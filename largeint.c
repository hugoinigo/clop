#include "largeint.h"
#include <malloc.h>

#define SIGN_NEG false
#define SIGN_POS !SIGN_NEG

void intn_init(intn_t *num, uint64_t size, ...)
{
	va_list ptr;
	uint64_t i, aux;
	
	num->data = calloc(size, sizeof(uint64_t));
	num->size = size;
	num->sign = SIGN_POS;

	va_start(ptr, size);
	for (i = 0; i < size; ++i) {
		aux = va_arg(ptr, uint64_t);
		num->data[i] = aux;
	}
	va_end(ptr);
}
void intn_dbg(const intn_t *num)
{
	uint64_t i;
	
	printf("intn addr = %p\n", num);
	printf("->size = %ld\n", num->size);
	printf("->sign = %s\n", num->sign == SIGN_POS ? "+ POS" : "- NEG");
	printf("->data = ");
	
	for (i = 0; i < num->size; ++i) {
		printf("%ld\n         ", num->data[i]);
	}
}

void intn_add(const intn_t *fst, const intn_t *snd)
{
}

void intn_sub(const intn_t *fst, const intn_t *snd)
{
}

void intn_mul(const intn_t *fst, const intn_t *snd)
{
}

void intn_div(const intn_t *fst, const intn_t *snd)
{
}

void intn_free(intn_t *num)
{
	free(num->data);
}
